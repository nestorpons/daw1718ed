README

#Nestor pons nestorpons@hotmail.es

Test prácticos para modulo DAW_1_ED 1718 
=========================================

Puedes hacer los test pinchando en este [link] (https://edtests.herokuapp.com/)

Para los alumnos de DAW_1_ED puedan hacer los ejercicios auto-evaluables de una manera un poco más interactiva


Colaboración 
--------------------

Espero que sea un proyecto colaborativo que nos sirva para aprender todos, comentar y conocernos todos un poco mejor. 
Libre y abierto a cualquier cambio. 

Test
--------------------

Los test que están el la carpeta test son los que se pueden seleccionar en el inicio. 
Iré subiendo al servidor todos los test que nos pongan en las evaluaciones. 

## Sintaxis de los archivos *.txt

Los tres primeros dobles símbolos de barra // son para titulo , créditos u otra información relevante. 
los siguientes, dobles símbolos de barra // , indican el inicio de una pregunta. Las respuestas se encuentran entre llaves {~respuesta 1, ~respuesta 2, =respuesta 3, ~respuesta n , .... } . 
Todas deben empezar con el símbolo ~ menos la respuesta correcta que empieza con el símbolo = . 
Indefinidas respuestas pero solo puede haber una respuesta correcta .
No hay numero mínimo ni máximo de preguntas.




