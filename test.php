<?php
session_start();
// clase Text que procesa los archivos .txt 
require_once 'Text.class.php' ;
$Text = new Text ('tests/' . $_GET['test']) ;

// metodo que pasa el argumento de lineas que deseamos que interprete como cabecera 
$tile_lines = 3 ;
$tiles =  $Text->getTile($tile_lines);
$questions =  $Text->getQuest($tile_lines);
$answers =  $Text->getAnswer($tile_lines);
?>
<!doctype html>
    <html lang="" >
        <head> 
            <meta charset="UTF-8">
            <link rel=stylesheet href='style.css'>
            <title></title>
        </head>
        <body>
            <h1><?= $tiles ?></h1>
            <?php 
                // Imprimimos lo que nos devuelve la clase 
                foreach($questions as $key => $question){
                    
                    $answer = $answers[$key] ;
                ?>

                    <fieldset>
                        <span class='quest'><?=$question?></span><br>
                        <div class='quest'>
                            <?php 
                            foreach ($answer as $k => $val){
                                $cls = $val[0] == '=' ? 'cls_true' :  'cls_false' ;
                                $answ = substr($val , 1); 
                                ?>
                                <label class = '<?=$cls?>'>
                                    <input type = 'radio' name="answer_<?=$key?>">
                                    <span ><?=$answ?></span>
                                </label>
                                <br>
                                <?php
                            }
                            
                            ?>
                        
                        </div>
                    </fieldset>
                    <br>
                <?php
                }
            ?>
            <input type='button' id = 'cmdRevise' value='Corregir'>
            <script src='index.js'></script>
        </body>
    </html>